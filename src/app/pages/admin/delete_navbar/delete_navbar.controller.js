(function () {
  'use strict';

  angular
    .module('delete_navbar.page')
    .controller('DeleteNavbarController', DeleteNavbarController);

  /** @ngInject */
  function DeleteNavbarController(localStorageService, messageCenterService) {
    var vm = this;

    vm.models = {
      selected: null,
      lists: {"A": [], "B": []}
    };

    //getting pages
    vm.models.lists.A = localStorageService.get('navbar_pages');

    //saving changes function
    vm.save = save;


    function save() {
      var unassigned = localStorageService.get('unassigned_pages');

      for(var i = 0, len = vm.models.lists.B.length; i < len; i++) {
        unassigned.push(vm.models.lists.B[i]);
      }
      localStorageService.set('unassigned_pages',unassigned);
      localStorageService.set('navbar_pages', vm.models.lists.A);

      messageCenterService.add('success', 'Zmiany zostaly zapisane! Odswiez strone aby zobaczyc zmiany.', {timeout: 3000});
    }
  }
})()
