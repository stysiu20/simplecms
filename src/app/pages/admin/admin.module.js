(function () {
  'use strict';

  angular
    .module('admin.page', [
      'left_menu.components',
      'new_page.page',
      'manage_navbar.page',
      'delete_page.page',
      'edit_page.page',
      'manage_new_users.page',
      'new_page_advanced.page',
      'files.page'
    ]);
})();
