(function () {
  'use strict';

  angular
    .module('new_page_advanced.page')
    .controller('NewPageAdvancedController', NewPageAdvancedController);

  /** @ngInject */
  function NewPageAdvancedController(messageCenterService, new_pageService, NewPageAdvancedService, $rootScope, GoogleMapsModal, TinyMCEModal, FilesModal, GalleryModal) {
    var vm = this;
    vm.data = {};

    vm.list1 = [
      {
        title: 'TinyMCE', html: ''
      },
      {
        title: 'GoogleMaps',
        html: "<ng-map zoom='13' center='Warszawa'></ng-map>"
      },
      {
        title: 'Files',
        html: "<file-list files='dynamicpage.files_list'></file-list>",
        files_list: ''
      },
      {
        title: 'Gallery',
        html: "<gallery-list images='dynamicpage.images'></gallery-list>",
        images: ''
      }];

    vm.page_parts = [];

    vm.dropCallbackHeader = function (event, ui) {
      console.log('modules', vm.list1);
    }

    vm.clearEditor = function () {
      vm.page_parts = [];
      vm.list1 = [
        {
          title: 'TinyMCE', html: ''
        },
        {
          title: 'GoogleMaps',
          html: "<ng-map zoom='13' center='Warszawa'></ng-map>"
        },
        {
          title: 'Files',
          html: "<file-list files='dynamicpage.files_list'></file-list>",
          files_list: ''
        },
        {
          title: 'Gallery',
          html: "<gallery-list images='dynamicpage.images'></gallery-list>",
          images: ''
        }];
    }

    vm.Add = function (obj) {
      if (obj === 'GoogleMaps') {
        GoogleMapsModal.resolve()
          .then(function (response) {
            var html = "<ng-map zoom='13' center='" + response.params.center + "' style='height:" + response.params.height + "px; width:" + response.params.width + "px; margin:0 auto;'></ng-map>";
            for (var i = 0, len = vm.page_parts.length; i < len; ++i) {
              if (vm.page_parts[i].title === obj) {
                vm.page_parts[i].html = html;
              }
            }
          });
      }
      else if (obj === 'TinyMCE') {
        TinyMCEModal.resolve()
          .then(function (response) {
            var html = response.params;
            for (var i = 0, len = vm.page_parts.length; i < len; ++i) {
              if (vm.page_parts[i].title === obj) {
                vm.page_parts[i].html = html;
              }
            }
            console.log('HTML', html);
            if (typeof html === "undefined") {
              messageCenterService.add('danger', ' Dodawanie tekstu nie powiodlo sie.Sprobuj ponownie.' +
                'Jesli widzisz ten komunikat po raz kolejny sprobuj dodac spacje na poczatku okna edycji',
                {timeout: 3000});
            }
          })
      }
      else if (obj === 'Files') {
        FilesModal.resolve()
          .then(function (response) {
            var html = response.params;
            for (var i = 0, len = vm.page_parts.length; i < len; ++i) {
              if (vm.page_parts[i].title === obj) {
                vm.page_parts[i].files_list = html;
              }
            }
          })
      }
      else if (obj === 'Gallery') {
        GalleryModal.resolve()
          .then(function (response) {
            var html = response.params;
            for (var i = 0, len = vm.page_parts.length; i < len; ++i) {
              if (vm.page_parts[i].title === obj) {
                vm.page_parts[i].images = html;
              }
            }
          })

      }
    }

    vm.addPage = function (data) {
      data = NewPageAdvancedService.Parser(data, vm.page_parts);
      if (new_pageService.isCorrectLink(data.link)) {
        //  console.log(data);
        new_pageService.createPage(data);

        //setting default
        vm.data = null;
        vm.page_parts = [];
        vm.list1 = [
          {
            title: 'TinyMCE', html: ''
          },
          {
            title: 'GoogleMaps',
            html: "<ng-map zoom='13' center='Warszawa'></ng-map>"
          },
          {
            title: 'Files',
            html: "<file-list files='dynamicpage.files_list'></file-list>",
            files_list: ''
          },
          {
            title: 'Gallery',
            html: "<gallery-list images='dynamicpage.images'></gallery-list>",
            images: ''
          }];
        vm.formPage.$setPristine();
        messageCenterService.add('success', 'Strona utworzona poprawnie!', {timeout: 3000});
        $rootScope.$broadcast('RefleshPages');
      }
      else {
        messageCenterService.add('danger', 'Strona o podanym linku juz istnieje!', {timeout: 3000});
      }
    }
  }
})()

