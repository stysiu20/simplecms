(function () {
  'use strict';

  angular
    .module('new_page_advanced.page')
    .factory('NewPageAdvancedService', NewPageAdvancedService);

  NewPageAdvancedService.$inject = ['localStorageService'];

  function NewPageAdvancedService(localStorageService) {
    var service = {};

    service.Parser = function (data, page) {
      var html_out = '';
      console.log('Data in parser in', page);
      for (var i = 0, len = page.length; i < len; i++) {
        if (page[i].html !== '') {
          html_out = html_out + ' ' + page[i].html;
        }
        if(page[i].title === 'Files') {
          data.files_list = page[i].files_list;
        }
        if(page[i].title === 'Gallery') {
          data.images = page[i].images;
        }
      }
      data.content = html_out;
      return data;
    }
    return service;
  }


})();
