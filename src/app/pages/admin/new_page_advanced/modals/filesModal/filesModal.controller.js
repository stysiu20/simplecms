(function () {
  'use strict';

  angular
    .module('new_page_advanced.modal')
    .controller('FilesModalController', FilesModalController);

  function FilesModalController($element, close, GetFilesService) {
    var vm = this;
    var outer_list = [];

    GetFilesService.get()
      .then(function (response) {
        vm.files_list = response;

      });
    vm.choose = function (file, checkbox) {
      if (checkbox) {
        outer_list.push(file);
      }
      else {
        for (var i = 0, len = outer_list.length; i < len; i++) {
          if (outer_list[i].createdOn === file.createdOn) {
            outer_list.splice(i, 1);
          }
        }
      }
    }

    vm.cancel = function () {
      $element.modal('hide');
    };

    vm.close = function () {
      close({
        params: outer_list
      }, 300);
    };
  }
})();
