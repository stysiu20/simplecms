(function () {
  'use strict';

  angular
    .module('new_page_advanced.modal')
    .controller('GalleryModalController', GalleryModalController);

  function GalleryModalController($element, close, GetFilesService) {
    var vm = this;
    var outer_list = [];
    vm.files = [{url: 'assets/images/house-02.jpg'}, {url: 'assets/images/house-03.jpg'}, {url: 'assets/images/house-06.jpg'}, {url: 'assets/images/house-08.jpg'}]


    GetFilesService.get()
      .then(function (response) {
        vm.files_list = response;
        console.log('asd', response);
      });

    vm.choose = function (file, checkbox) {
      if (checkbox) {
        var obj = {};
        obj.img =  file.url;
        obj.name = 'img';
        outer_list.push(obj);
      }
      else {
        for (var i = 0, len = outer_list.length; i < len; i++) {
          if (outer_list[i].file.url === file.file.url) {
            outer_list.splice(i, 1);
          }
        }
      }
    }

    vm.cancel = function () {
      $element.modal('hide');
    };

    vm.close = function () {
      close({
        params: outer_list
      }, 300);
    };
  }
})();
