(function () {
  'use strict';

  angular
    .module('new_page_advanced.modal')
    .controller('GoogleMapsModalController', GoogleMapsModalController);

  function GoogleMapsModalController($element, close) {
    var vm = this;

    vm.cancel = function () {
      $element.modal('hide');
    };

    vm.close = function() {
      close({
        params: vm.data
      }, 300);
    };
  }
})();
