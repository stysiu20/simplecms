(function () {
  'use strict';

  angular
    .module('new_page_advanced.modal')
    .controller('tinyMCEModalController', tinyMCEModalController);

  function tinyMCEModalController($element, close) {
    var vm = this;


    vm.tinymceOptions = {
      plugins: 'advlist autolink link image lists charmap print preview',
      height: 300

    }
    vm.cancel = function () {
      $element.modal('hide');
    };

    vm.close = function() {
      close({
        params: vm.content
      }, 300);
    };
  }
})();
