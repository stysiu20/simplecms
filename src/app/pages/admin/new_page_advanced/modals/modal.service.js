(function () {
  'use strict';

  angular
    .module('new_page_advanced.modal')
    .factory('TinyMCEModal', TinyMCEModal)
    .factory('FilesModal', FilesModal)
    .factory('GalleryModal', GalleryModal)
    .factory('GoogleMapsModal', GoogleMapsModal);

  function TinyMCEModal(ModalService) {
    return {
      resolve: function () {
        return ModalService.showModal({
          templateUrl: "app/pages/admin/new_page_advanced/modals/tinyMCE/tinyMCEModal.tpl.html",
          controller: "tinyMCEModalController",
          controllerAs: "tiny"
        }).then(function (modal) {
          modal.element.modal();
          return modal.close.then(function (result) {
            return result;
          })
        });
      }
    }
  }


  function GalleryModal(ModalService) {
    return {
      resolve: function () {
        return ModalService.showModal({
          templateUrl: "app/pages/admin/new_page_advanced/modals/Gallery/galleryModal.tpl.html",
          controller: "GalleryModalController",
          controllerAs: "gallery"
        }).then(function (modal) {
          modal.element.modal();
          return modal.close.then(function (result) {
            return result;
          })
        });
      }
    }
  }

  function FilesModal(ModalService) {
    return {
      resolve: function () {
        return ModalService.showModal({
          templateUrl: "app/pages/admin/new_page_advanced/modals/filesModal/filesModal.tpl.html",
          controller: "FilesModalController",
          controllerAs: "file"
        }).then(function (modal) {
          modal.element.modal();
          return modal.close.then(function (result) {
            return result;
          })
        });
      }
    }
  }


  function GoogleMapsModal(ModalService) {
    return {
      resolve: function () {
        return ModalService.showModal({
          templateUrl: "app/pages/admin/new_page_advanced/modals/googleMaps/googleMapsModal.tpl.html",
          controller: "GoogleMapsModalController",
          controllerAs: "mapscontroller"
        }).then(function (modal) {
          modal.element.modal();
          return modal.close.then(function (result) {
            return result;
          })
        });
      }
    }
  }

})();
