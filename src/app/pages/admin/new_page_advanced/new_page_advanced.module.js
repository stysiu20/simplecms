(function () {
  'use strict';

  angular
    .module('new_page_advanced.page', ['googleMaps.page.components','new_page_advanced.modal']);
})();
