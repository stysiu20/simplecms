(function () {
  'use strict';

  angular
    .module('get_files.page')
    .controller('GetFilesController', GetFilesController)
    .factory('GetFilesService', GetFilesService);

  /** @ngInject */
  function GetFilesController(GetFilesService) {
    var vm = this;

    GetFilesService.get().then(function (response) {
      vm.files_list = response;
    });

    vm.reflesh = function () {
      GetFilesService.get()
        .then(function (response) {
          vm.files_list = response;
          console.log(response);
        });


    }
  }

  function GetFilesService($http) {
    return {
      get: function () {
        return $http.get('http://ec2-52-58-109-154.eu-central-1.compute.amazonaws.com:8080/files')
          .then(function (response) {
            return response.data;
          })
      }
    }
  }
})();
