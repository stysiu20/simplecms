(function () {
  'use strict';

  angular
    .module('files.page', ['send_files.page','get_files.page']);
})();
