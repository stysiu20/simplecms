(function () {
  'use strict';

  angular
    .module('send_files.page', ['ngFileUpload']);
})();
