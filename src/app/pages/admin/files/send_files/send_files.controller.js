(function () {
  'use strict';

  angular
    .module('send_files.page')
    .controller('SendFilesController', SendFilesController);

  /** @ngInject */
  function SendFilesController(Upload) {
    var vm = this;
    vm.errorMsg = null;
    vm.progress = null;

    vm.FileDroped = function (file) {
      vm.UploadFiles(file);
    }
    vm.UploadFiles = function (file) {
      if (file) {
        vm.errorMsg = null;
        vm.progress = null;
        console.log('Start', 'er mess', vm.errorMsg);
        Upload.upload({
          url: 'http://ec2-52-58-109-154.eu-central-1.compute.amazonaws.com:8080/files',
          data: {
            file: file
          }
        }).then(function (response) {
          console.log(vm.result);
          vm.result = response.data;
        }, function (response) {
          if (response.status > 0) {
            vm.errorMsg = 'Niepowodzenie - sprobuj ponownie.';
          }
        }, function (evt) {
          vm.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
    }
  }
})();
