(function () {
  'use strict';

  angular
    .module('manage_new_users.page')
    .factory('ConfirmUserService', ConfirmUserService)
    .factory('DeleteAcceptedUser', DeleteAcceptedUser);

  ConfirmUserService.$inject = ['localStorageService'];

  function ConfirmUserService(localStorageService) {
    return {
      resolve: function (user) {
        var confirmed_users = localStorageService.get('register_users');
        if (confirmed_users === null) {
          confirmed_users = [];
          confirmed_users.push(user);
        } else {
          confirmed_users.push(user);
        }
        localStorageService.set('register_users', confirmed_users);
      }
    }
  }

  function DeleteAcceptedUser(localStorageService) {
    return {
      resolve: function (user) {
        var new_users = localStorageService.get('new_users');

        for (var i = 0, len = new_users.length; i < len; i++) {
          if (new_users[i].login === user.login) {
            new_users.splice(i, 1);
            break;
          }
        }
        localStorageService.set('new_users', new_users);
      }
    }
  }
})();
