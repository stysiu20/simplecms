(function () {
  'use strict';

  angular
    .module('manage_new_users.page')
    .controller('ManageNewUsersController', ManageNewUsersController);

  /** @ngInject */
  function ManageNewUsersController(localStorageService, ConfirmUserService, DeleteAcceptedUser) {
    var vm = this;

    vm.new_users = localStorageService.get('new_users');

    vm.accept = function(user) {
      console.log('akceptuje',user);
      ConfirmUserService.resolve(user);
      DeleteAcceptedUser.resolve(user);

      vm.new_users = localStorageService.get('new_users');
    }
  }
})();
