(function () {
  'use strict';

  angular
    .module('manage_navbar.page')
    .controller('ManageNavbarController', ManageNavbarController);

  /** @ngInject */
  function ManageNavbarController(localStorageService, messageCenterService) {

    var vm = this;

    vm.models = {
      selected: null,
      lists: {"A": [], "B": []}
    };

   //getting pages
    vm.models.lists.A = localStorageService.get('unassigned_pages');
    vm.models.lists.B = localStorageService.get('navbar_pages');
    vm.models.lists.C = localStorageService.get('pages');

    //saving changes function
    vm.save = save;

    if (vm.models.lists.B === null) {
      vm.models.lists.B = [];
    }

    function save() {
      localStorageService.set('unassigned_pages',vm.models.lists.A);
      localStorageService.set('navbar_pages',vm.models.lists.B);
      messageCenterService.add('success', 'Zmiany zostaly zapisane! Odswiez strone aby zobaczyc zmiany.', { timeout: 3000 });
    }
  }
})()
