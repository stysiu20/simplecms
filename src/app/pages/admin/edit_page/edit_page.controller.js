(function () {
  'use strict';

  angular
    .module('edit_page.page')
    .controller('EditPageController', EditPageController);

  /** @ngInject */
  function EditPageController(messageCenterService, GetDataPageService, SetDataPageChangesService) {
    var vm = this;
    vm.data = {};

    vm.tinymceOptions = {
      plugins: 'advlist autolink link image lists charmap print preview',
      height: 300

    }

    GetDataPageService.resolve()
      .then(function (response) {
        vm.data = response;
      });

    vm.editPage = function (data) {
      SetDataPageChangesService.save(data);
      messageCenterService.add('success', 'Strona edytowana poprawnie!', {timeout: 3000});
    }
  }
})()
