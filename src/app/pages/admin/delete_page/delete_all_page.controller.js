(function () {
  'use strict';

  angular
    .module('delete_page.page')
    .controller('DeletePageController', DeletePageController);

  /** @ngInject */
  function DeletePageController(localStorageService, $rootScope, messageCenterService) {
    var vm = this;

    vm.models = {
      selected: null,
      lists: {"A": []}
    };

    //getting pages
    vm.models.lists.A = localStorageService.get('pages');

    vm.save = save;

    //$scope.$watch('deletepage.models', function (model) {
    //
    //  //vm.modelAsJson = angular.toJson(model, true);
    //  console.log('List a', vm.models.lists.A);
    //  console.log('list b', vm.models.lists.B);
    //}, true);


    function save() {
      localStorageService.set('unassigned_pages', vm.models.lists.A);
      localStorageService.set('pages', vm.models.lists.A);

      messageCenterService.add('success', 'Zmiany zostaly zapisane!', {timeout: 3000});
      $rootScope.$broadcast('RefleshPages');
    }
  }
})()
