(function () {
  'use strict';

  angular
    .module('login.page')
    .controller('AdminPageController', AdminPageController);

  /** @ngInject */
  function AdminPageController() {
    //this is admin page controller
    var vm = this;
    vm.labels = [];
    vm.data = [];
    var points = [];

    //iniciation chart
    for (var i = 1; i <= 30; i++) {
      vm.labels.push(i);
      points.push(Math.floor((Math.random() * 100) + 1));
    }

    vm.series = ['Liczba wejść na dzień'];
    vm.data.push(points);

    vm.data_pie = [30, 50, 80, 70, 100, 10, 5];
    vm.labels_pie = ["Polska", "Rosja", "Norwegia", "USA", "Canada", "Ukraina", "Inne"];

    vm.labels_don = ["Użycie przestrzeni dyskowe [%]", "Wolne [%]"];
    vm.data_don = [78,22];
  }


})()
