(function () {
  'use strict';

  angular
    .module('new_page.page')
    .controller('new_pageController', new_pageController);

  /** @ngInject */
  function new_pageController(new_pageService, messageCenterService, $rootScope) {
    var vm = this;

    vm.tinymceOptions = {
      plugins: 'advlist autolink link image lists charmap print preview',
      height: 300

    }

    vm.addPage = function (data) {
      if (typeof vm.data.content === "undefined") {
        messageCenterService.add('danger', 'Dodawanie tekstu nie powiodlo sie. Sprobuj ponownie.' +
          'Jesli widzisz ten komunikat po raz kolejny sprobuj dodac spacje na poczatku okna edycji', {timeout: 3000});
      }
      else {
        if (new_pageService.isCorrectLink(data.link)) {
          data.restriction = '0';
          new_pageService.createPage(data);
          vm.data = null;
          vm.formPage.$setPristine();
          messageCenterService.add('success', 'Strona utworzona poprawnie!', {timeout: 3000});
          $rootScope.$broadcast('RefleshPages');
        }
        else {
          messageCenterService.add('danger', 'Strona o podanym linku juz istnieje!', {timeout: 3000});
        }
      }
    }
  }
})()
