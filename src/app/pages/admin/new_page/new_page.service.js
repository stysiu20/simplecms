(function () {
  'use strict';

  angular
    .module('new_page.page')
    .factory('new_pageService', new_pageService);

  new_pageService.$inject = ['localStorageService'];

  function new_pageService(localStorageService) {
    var service = {};

    service.createPage = function (data_page) {

      var all_pages = localStorageService.get('pages');
      var unassinged_pages = localStorageService.get('unassigned_pages');

      if (all_pages === null || all_pages == []) {
        all_pages = [];
        all_pages.push(data_page);
        localStorageService.set('pages', all_pages);
        localStorageService.set('unassigned_pages', all_pages);

      }
      else {

        all_pages.push(data_page);
        unassinged_pages.push(data_page);
        localStorageService.set('pages', all_pages);
        localStorageService.set('unassigned_pages', unassinged_pages);

      }
    }
    service.isCorrectLink = function (link) {
      var all_pages = localStorageService.get('pages');
      if (all_pages === null) {
        return true;
      }
      else {
        for (var i = 0, len = all_pages.length; i < len; i++) {
          if (link === all_pages[i].link) {
            return false;
          }
        }
        return true;
      }
    }

    return service;
  }


})();
