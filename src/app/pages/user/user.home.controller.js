(function() {
  'use strict';

  angular
    .module('login.page')
    .controller('UserPageController', UserPageController);

  /** @ngInject */
  function UserPageController() {
    //this is user page controller
  }
})();
