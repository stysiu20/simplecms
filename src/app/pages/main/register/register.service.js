(function () {
  'use strict';

  angular
    .module('register.page')
    .factory('RegisterService', RegisterService);


  RegisterService.$inject = ['$q', 'localStorageService'];
  function RegisterService($q, localStorageService) {
    var service = {};

    service.isCorrectLogin = function (data) {
      var new_users = [];
      var registered_users = [];
      var deffered = $q.defer();

      if (localStorageService.get('register_users') === null) {
        if (localStorageService.get('new_users') === null) {
          new_users = [];
          new_users.push(data);
          deffered.resolve(data);
        }
        else {
          new_users = localStorageService.get('new_users');

          for (var i = 0, len = new_users.length; i < len; i++) {
            if (new_users[i].login === data.login) {
              deffered.reject('JUZ TAKI ISTNIEJE');
              break;
            }
          }
          deffered.resolve(data);
        }
      }
      else {
        registered_users = localStorageService.get('register_users');
        if (localStorageService.get('new_users') === null || localStorageService.get('new_users').length === 0) {
          for (var i = 0, len2 = registered_users.length; i < len2; i++) {
            if (registered_users[i].login === data.login) {
              deffered.reject('JUZ TAKI ISTNIEJE');
              break;
            }
          }
          deffered.resolve(data);
        }
        else {
          for (var i = 0, len = registered_users.length; i < len; i++) {
            if (registered_users[i].login === data.login) {
              deffered.reject('JUZ TAKI ISTNIEJE');
              break;
            }
          }
          new_users = localStorageService.get('new_users');
          for (var i = 0, len = new_users.length; i < len; i++) {
            if (new_users[i].login === data.login) {
              deffered.reject('JUZ TAKI ISTNIEJE');
              break;
            }
          }
          deffered.resolve(data);
        }
      }
      return deffered.promise;
    }

    service.save = function (data) {
      var new_users = localStorageService.get('new_users');

      if (new_users === null) {
        new_users = [];
        new_users.push(data);
      }
      else {
        new_users.push(data);
      }
      localStorageService.set('new_users', new_users);
    };

    return service;
  }
})();
