(function () {
  'use strict';

  angular
    .module('register.page')
    .controller('RegisterController', RegisterController);

  /** @ngInject */
  function RegisterController(RegisterService, $timeout, messageCenterService) {
    var vm = this;

    vm.register = function (data) {
      if (data.password === vm.second_password) {
        var role = 'user';
        data.register_time = new Date();
        ;
        data.role = role;

        RegisterService.isCorrectLogin(data)
          .then(function (response) {
            RegisterService.save(response);

            messageCenterService.add('success', 'Zarejestrowano poprawnie!', {timeout: 3000});
            vm.data = null;
            vm.second_password = null;
          }, function (res) {
            messageCenterService.add('danger', 'Ten login jest juz zajety!', {timeout: 3000});
          });

        $timeout(function () {
          vm.register_correct = false;
        }, 4000);
      }
      else {
        messageCenterService.add('danger', 'Hasla sa rozne!', {timeout: 3000});
      }
    };


  }
})();
