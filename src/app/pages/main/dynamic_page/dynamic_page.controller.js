(function () {
  'use strict';

  angular
    .module('dynamic_page.page')
    .controller('DynamicPageController', DynamicPageController);

  /** @ngInject */
  function DynamicPageController(GetDataPageService) {
    var vm = this;
    vm.data_page = {};

    GetDataPageService.resolve()
      .then(function (response) {
        vm.data_page = response;
        vm.files_list = vm.data_page.files_list;
        vm.images = vm.data_page.images;
        console.log('In dir images', vm.images);
      });
  }
})();
