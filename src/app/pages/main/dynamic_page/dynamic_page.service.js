(function () {
  'use strict';

  angular
    .module('dynamic_page.page')
    .factory('GetDataPageService', GetDataPageService)
    .factory('SetDataPageChangesService', SetDataPageChangesService)
    .directive('compile', compile);

  function compile($compile) {
    return function (scope, element, attrs) {
      scope.$watch(
        function (scope) {
          // watch the 'compile' expression for changes
          return scope.$eval(attrs.compile);
        },
        function (value) {
          // when the 'compile' expression changes
          // assign it into the current DOM
          element.html(value);

          // compile the new DOM and link it to the current
          // scope.
          // NOTE: we only compile .childNodes so that
          // we don't get into infinite loop compiling ourselves
          $compile(element.contents())(scope);
        }
      );
    };
  }

  GetDataPageService.$inject = ['$q', 'localStorageService', '$stateParams'];
  function GetDataPageService($q, localStorageService, $stateParams) {
    return {
      resolve: function () {
        console.log('State param z servisu', $stateParams.pageName);
        var deffered = $q.defer();
        var link = $stateParams.pageName;
        var pages = localStorageService.get('pages');
        for (var i = 0, len = pages.length; i < len; i++) {
          if (pages[i].link === link) {
            deffered.resolve(pages[i]);
          }
        }
        return deffered.promise;
      }
    }
  }


  SetDataPageChangesService.$inject = ['localStorageService'];
  function SetDataPageChangesService(localStorageService) {
    return {
      save: function (data) {
        var pages = localStorageService.get('pages');
        for (var i = 0, len = pages.length; i < len; i++) {
          if (data.link == pages[i].link) {
            pages[i].content = data.content;
          }
        }
        localStorageService.set('pages', pages);
      }
    }
  }
})();
