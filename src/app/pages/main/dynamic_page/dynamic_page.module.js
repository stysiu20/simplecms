(function () {
  'use strict';

  angular
    .module('dynamic_page.page', ['tinyEditor.page.components','gallery.components']);
})();
