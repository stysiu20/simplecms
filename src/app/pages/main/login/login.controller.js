(function() {
  'use strict';

  angular
    .module('login.page')
    .controller('LoginPageController', LoginPageController);

  /** @ngInject */
  function LoginPageController() {
    //this is login page controller
  }
})()
