(function () {
  'use strict';

  angular
    .module('contact.page')
    .controller('ContactController', ContactController);

  /** @ngInject */
  function ContactController() {
    var vm = this;

    vm.address = "Toronto Canada";
    vm.placement = {
      place: [
        {title: 'Rzeszów', adress: 'Generała Stanisława Maczka 6, Rzeszów'},
        {title: 'Wrocław', adress: 'Klecińska 123, 54-413 Wrocław'},
        {title: 'Gdańsk', adress: 'Jana z Kolna 11, Gdańsk'}
      ],
      selected: {title: 'Rzeszów', adress: 'Generała Stanisława Maczka 6, Rzeszów'}
    };
  }
})();
