(function () {
  'use strict';

  angular
    .module('login.components')
    .directive('loginPanel', loginPanel);

  /** @ngInject */
  function loginPanel() {
    var directive = {
      templateUrl: 'app/components/login/login.tpl.html',
      controller: loginPanelController,
      controllerAs: 'loginpanel',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function loginPanelController(AuthorizationService, UserService, $timeout, $state, $rootScope, AUTH_EVENTS) {
      var vm = this;
      vm.loader = false;
      vm.failedlogin = false;

      vm.login = function (credentials) {
        vm.loader = true;
        AuthorizationService.login(credentials)
          .then(function (response) {
            vm.loader = false;

            UserService.setUser(credentials);
            if (response === 'admin') {
              $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
              $state.go('admin')
            }
            else {
              $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
              $state.go('home')
            }

          }, function (response) {
            vm.loader = false;
            vm.failedlogin = true;
            vm.credentials = null;

            $timeout(function () {
              vm.failedlogin = false
            }, 3000);
          });
      }


    }
  }
})();
