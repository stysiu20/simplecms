(function () {
  'use strict';

  angular
    .module('login.components')
    .factory('AuthorizationService', AuthorizationService)
    .factory('UserService', UserService);

  AuthorizationService.$inject = ['$q', 'ROLES', '$timeout', 'localStorageService'];

  function AuthorizationService($q, ROLES, $timeout, localStorageService) {
    return {
      login: function (credentials) {
        var deffered = $q.defer();


        $timeout(function () {
          var users = localStorageService.get('register_users');
          if (users === null) {
            var user = {login: 'user', password: 'user'};
            users = [];
            users.push(user);
          }

          for (var i = 0, len = users.length; i < len; i++) {

            if (credentials.login === 'admin' && credentials.password === 'admin') {
              deffered.resolve(ROLES.admin);
            }
            else if (credentials.login === users[i].login && credentials.password === users[i].password) {
              deffered.resolve(ROLES.user);
            }
            else {
              deffered.reject('Niepoprawny login i/lub hasło');
            }
          }
        }, 1000)
        return deffered.promise;
      }
    }
  }

  UserService.$inject = ['localStorageService'];
  function UserService(localStorageService) {
    var service = {};

    service.setUser = function (credentials) {
      var user = {
        'name': credentials.login,
        'role': credentials.login
      };

      localStorageService.set('currentUser', user);
    };
    service.getUser = function () {
      return localStorageService.get('currentUser');
    }

    return service;
  }

})
();
