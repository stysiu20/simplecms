(function () {
  angular
    .module('fileList.components')
    .directive('fileList', fileList);

  /** @ngInject */
  function fileList() {
    var directive = {
      restrict:'EAC',
      templateUrl: 'app/components/admin/fileList/fileList.tpl.html',
      controller: fileListController,
      controllerAs: 'file',
      bindToController: true,
      scope: {
        files: '='
      }
    };

    return directive;

    /** @ngInject */
    function fileListController(GetFilesService) {
      var vm = this;
      console.log('FIles list in directive', vm.files);
      vm.files_list = vm.files;
      //GetFilesService.get().then(function (response) {
      //  vm.files_list = response;
      //});

    }
  }

})()
