(function () {
  angular
    .module('tinyEditor.page.components')
    .directive('tinyEditor', tinyEditor);

  /** @ngInject */
  function tinyEditor() {
    var directive = {
      templateUrl: 'app/components/admin/tinyEditorPage/tinyEditor.page.html',
      controller: tinyEditorController,
      controllerAs: 'tiny',
      bindToController: true,
      scope: {
        output: '='
      }
    };

    return directive;

    /** @ngInject */
    function tinyEditorController(localStorageService, $rootScope) {
      var vm = this;

      vm.tinymceOptions = {
        plugins: 'advlist autolink link image lists charmap print preview',
        height: 300

      }

      vm.Log = function () {
        console.log('From TIny direcitvce', vm.data.content);
        vm.output = vm.data.content;
      }
    }
  }

})()
