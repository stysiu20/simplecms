(function () {
  'use strict';

  angular
    .module('left_menu.components')
    .directive('leftMenu', leftMenu);

  /** @ngInject */
  function leftMenu() {
    var directive = {
      templateUrl: 'app/components/admin/left_menu/left_menu.tpl.html',
      controller: leftMenuController,
      controllerAs: 'leftmenu',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function leftMenuController(localStorageService, $rootScope) {
      var vm = this;
      vm.pages_list = localStorageService.get('pages');

      $rootScope.$on('RefleshPages', function () {
        vm.pages_list = localStorageService.get('pages');
      })


    }
  }
})();
