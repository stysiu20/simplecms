(function () {
  angular
    .module('gallery.components')
    .directive('galleryList', galleryList);

  /** @ngInject */
  function galleryList() {
    var directive = {
      restrict:'EAC',
      templateUrl: 'app/components/admin/gallery/gallery.tpl.html',
      controller: GalleryListController,
      controllerAs: 'gallery',
      bindToController: true,
      scope: {
        images: '='
      }
    };

    return directive;

    /** @ngInject */
    function GalleryListController() {
      var vm = this;

      vm.images_list = vm.images;

    }
  }

})()
