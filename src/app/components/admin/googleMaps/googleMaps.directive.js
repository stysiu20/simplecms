(function () {
  angular
    .module('googleMaps.page.components')
    .directive('googleMaps', googleMaps);

  /** @ngInject */
  function googleMaps() {
    var directive = {
      restrict:'EAC',
      templateUrl: 'app/components/admin/googleMaps/googleMaps.page.html',
      controller: GoogleMapsController,
      controllerAs: 'googlemaps',
      bindToController: true,
      scope: {
        maps_width: '=',
        maps_height: '=',
        maps_center: '='
      }
    };

    return directive;

    /** @ngInject */
    function GoogleMapsController() {
      var vm = this;
      console.log( vm.maps_center);
      //vm.maps_height = 300;
      //vm.maps_width = 300;
      //vm.size = "height:" + vm.maps_height + "px;" + "width:" + vm.maps_width + "px;";
      console.log(vm.maps_center);
    }
  }

})()
