(function () {
  'use strict';

  angular
    .module('simpleCms')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
        creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(localStorageService, UserService, $state, AUTH_EVENTS, $rootScope) {
      var vm = this;

      vm.main_list = localStorageService.get('navbar_pages');

      if(UserService.getUser() !== null) {
        $rootScope.loged = true;
      }
      else {
        $rootScope.loged = false;
      }

      $rootScope.$on(AUTH_EVENTS.loginSuccess, function() {
        $rootScope.loged = true;

      });
      $rootScope.$on(AUTH_EVENTS.loginFailed, function() {
        $rootScope.loged = false;
      });


      vm.logout = function () {
        localStorageService.remove('currentUser');
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        $state.go('home');
      }
    }
  }

})();
