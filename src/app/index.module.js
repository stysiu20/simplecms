(function() {
  'use strict';

  angular
    .module('simpleCms', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ui.router',
      'ui.bootstrap',
      'ui.tinymce',
      'toastr',
      'MessageCenterModule',
      'LocalStorageModule',
      'dndLists',
      'ngDragDrop',
      'chart.js',
      'ngMap',
      'angularModalService',
      'main.page',
      'login.page',
      'user.page',
      'admin.page'

    ]);

})();
