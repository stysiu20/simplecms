(function() {
  'use strict';

  angular
    .module('simpleCms')
    .config(config)
    .config(configLocalStorage);
    //.config(configGoogleMaps);

  /** @ngInject */
  function config($logProvider, toastrConfig) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
  }

  /** @ngInject */
  function configLocalStorage(localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('simpleCms')
      .setStorageType('localStorage')
      .setNotify(true, true)
  }

  /** @ngInject */
  //function configGoogleMaps(uiGmapGoogleMapApiProvider) {
  //  uiGmapGoogleMapApiProvider.configure({
  //    //    key: 'your api key',
  //    v: '3.20', //defaults to latest 3.X anyhow
  //    libraries: 'weather,geometry,visualization'
  //  });
  //}
})();
