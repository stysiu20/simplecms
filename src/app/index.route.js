(function () {
  'use strict';

  angular
    .module('simpleCms')
    .config(routerConfig)
    .run(StateChange);

  /** @ngInject */

  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/pages/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('page', {
        url: '/page/:pageName',
        templateUrl: 'app/pages/main/dynamic_page/dynamic_page.page.html',
        controller: 'DynamicPageController',
        controllerAs: 'dynamicpage'
        //resolve: {
        //  allowUser: function (localStorageService, $state) {
        //    var pages = localStorageService.get('pages');
        //    console.log($state);
        //    for (var i = 0, len = pages.length; i < len; i++) {
        //      if (pages[i].link === pageName) {
        //        pages[i].restriction = '0';
        //        return pages[i].restriction;
        //      }
        //    }
        //  }
        //}
      })
      .state('register', {
        url: '/register',
        templateUrl: 'app/pages/main/register/register.page.html',
        controller: 'RegisterController',
        controllerAs: 'register'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/pages/main/login/login.page.html',
        controller: 'LoginPageController',
        controllerAs: 'login'
      })
      .state('contact', {
        url: '/contact',
        templateUrl: 'app/pages/main/contact/contact.html',
        controller: 'ContactController',
        controllerAs: 'contact'
      })
      .state('admin', {
        url: '/admin',
        templateUrl: 'app/pages/admin/admin.home.page.html',
        controller: 'AdminPageController',
        controllerAs: 'adminpage'
      })
      //for future
      .state('admin.text', {
        url: '/text',
        views: {
          '': {templateUrl: 'app/pages/admin/text/admin.text.html'},
          'charts@admin.text': {templateUrl: 'app/pages/admin/manage_navbar/manage_navbar.html'},
          'recommendations@admin.dane': {templateUrl: 'app/pages/admin/delete_navbar/delete_navbar.html'},
          'users@admin.dane': {}
        }
      })
      .state('admin.files', {
        url: '/files',
        views: {
          '': {templateUrl: 'app/pages/admin/files/files.html'},
          'send@admin.files': {
            templateUrl: 'app/pages/admin/files/send_files/send_files.html',
            controller: 'SendFilesController',
            controllerAs: 'send',
          },
          'get@admin.files': {
            templateUrl: 'app/pages/admin/files/get_files/get_files.html',
            controller: 'GetFilesController',
            controllerAs: 'get'
            //resolve: {
            //  getFiles: function (GetFilesService) {
            //    GetFilesService.get()
            //      .then(function (response) {
            //        console.log('reouter',response);
            //        return response;
            //      });
            //  }
            //}
          }
        }
      })
      .state('admin.new_page', {
        url: '/new_page',
        templateUrl: 'app/pages/admin/new_page/new_page.page.html',
        controller: 'new_pageController',
        controllerAs: 'new_page'
      })
      .state('admin.advanced', {
        url: '/new_page/advanced',
        templateUrl: 'app/pages/admin/new_page_advanced/new_page_advanced.html',
        controller: 'NewPageAdvancedController',
        controllerAs: 'newadvanced'
      })
      .state('admin.delete_all', {
        url: '/delete_all',
        templateUrl: 'app/pages/admin/delete_page/delete_all_page.html',
        controller: 'DeletePageController',
        controllerAs: 'deletepage'
      })
      .state('admin.delete_navbar', {
        url: '/delete_navbar',
        templateUrl: 'app/pages/admin/delete_navbar/delete_navbar.html',
        controller: 'DeleteNavbarController',
        controllerAs: 'deletenavbar'

      })
      .state('admin.manage_navbar', {
        url: '/manage_navbar',
        templateUrl: 'app/pages/admin/manage_navbar/manage_navbar.html',
        controller: 'ManageNavbarController',
        controllerAs: 'managepage'
      })
      .state('admin.manage_new_users', {
        url: '/manage_new_users',
        templateUrl: 'app/pages/admin/manage_new_users/manage_new_users.html',
        controller: 'ManageNewUsersController',
        controllerAs: 'manageusers'
      })
      .state('user', {
        url: '/user/home',
        templateUrl: 'app/pages/user/user.home.page.html',
        controller: 'UserPageController',
        controllerAs: 'userpage'
      })
      .state('admin.edit_page', {
        url: '/edit_page/:pageName',
        templateUrl: 'app/pages/admin/edit_page/edit_page.html',
        controller: 'EditPageController',
        controllerAs: 'edit_page'
      });

    $urlRouterProvider.otherwise('/');
  }


  StateChange.$inject = ['$rootScope', 'UserService', '$state', 'localStorageService'];

  function StateChange($rootScope, UserService, $state, localStorageService) {

    $rootScope.$on('$stateChangeStart', function (event, next, toParams) {
      var currentUser = UserService.getUser();

      console.log(toParams);
      var pages = localStorageService.get('pages');
      var restriction = '';
      for (var i = 0, len = pages.length; i < len; i++) {
        if (pages[i].link === toParams.pageName) {
          restriction = pages[i].restriction;
        }
      }
      //if page is not restricted return false
      var restrictedPage = $.inArray(next.url, ['/login', '/register', '/', '/contact']) === -1;


      //if User is not loged and page is restricted  go into
      if (!currentUser && restrictedPage) {
        event.preventDefault();
        $state.go('home');
        console.log('Niepoprawnie');
      }
      //if(currentUser.role !== restriction || restriction==='0'){
      //  event.preventDefault();
      //  $state.go('home');
      //  console.log('Niepoprawnie');
      //}

    });
  }
})();
