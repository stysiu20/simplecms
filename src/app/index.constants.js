/* global malarkey:false, moment:false */
(function () {
  'use strict';

  angular
    .module('simpleCms')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('ROLES', {
      all: '*',
      admin: 'admin',
      user: 'user'
    })
    .constant('AUTH_EVENTS', {
      loginSuccess: 'auth-login-success',
      loginFailed: 'auth-login-failed'
    });

})();
