(function() {
  'use strict';

  angular
    .module('simpleCms')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
